#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# Function to check if Firefox Developer Edition is installed
is_firefox_dev_installed() {
    if command -v firefox-devedition &> /dev/null; then
        return 0
    else
        return 1
    fi
}

# Check if Firefox Developer Edition is already installed
if is_firefox_dev_installed; then
    echo "Firefox Developer Edition is already installed."
    exit 0
fi

echo "Installing Firefox Developer Edition..."

# Create directory for keyrings if it does not exist
sudo install -d -m 0755 /etc/apt/keyrings

# Download and save keyring
if ! wget -q "https://packages.mozilla.org/apt/repo-signing-key.gpg" -O- | sudo tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null; then
    echo "Error: Failed to download and save the Mozilla keyring." >&2
    exit 1
fi

# Add the Mozilla APT repository to your sources list:
if ! echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | sudo tee -a /etc/apt/sources.list.d/mozilla.list > /dev/null; then
    echo "Error: Failed to add Mozilla APT repository to sources list." >&2
    exit 1
fi

# Update your package list and install the Firefox .deb package:
echo "Updating package list..."
if ! sudo apt-get update; then
    echo "Error: Failed to update package list." >&2
    exit 1
fi

echo "Installing Firefox Developer Edition..."
if ! sudo apt-get install -y firefox-devedition; then
    echo "Error: Failed to install Firefox Developer Edition." >&2
    exit 1
fi

echo "Firefox Developer Edition has been successfully installed!"
