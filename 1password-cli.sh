#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
trap 'rm -f "$temp_file"' EXIT
IFS=$'\n\t'

# Check if dpkg is installed
if ! command -v dpkg >/dev/null 2>&1; then
  echo "dpkg is not installed. Please install it first."
  exit 1
fi

architecture=$(dpkg --print-architecture)
package_name="1password-cli-$architecture-latest.deb"
download_url="https://downloads.1password.com/linux/debian/$architecture/stable/$package_name"
temp_file="/tmp/$package_name"

# Download the .deb file to the temporary file
if ! wget -O "$temp_file" "$download_url"; then
  echo "Failed to download $package_name"
  exit 1
fi

# Install the .deb file using dpkg
if ! dpkg -i "$temp_file"; then
  echo "Failed to install $package_name"
  exit 1
fi

# Check the installed version of the 1Password CLI
op --version
