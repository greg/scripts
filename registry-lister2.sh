#!/usr/bin/env bash

# This script fetches and enumerates images in a given GitLab project.

set -euo pipefail

# Function to handle errors and provide clear feedback
handle_error() {
    echo "Error on line $LINENO. Exit code: $?" >&2
    exit 1
}

trap handle_error ERR

# Function to check if a command is available
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Check if necessary commands are available
for CMD in glab jq; do
    if ! command_exists "$CMD"; then
        printf "%s is not installed. Please install it first.\n" "$CMD"
        exit 1
    fi
done

# Prompt for PROJECT_ID if it's not set
prompt_for_project_id() {
    read -r -p "Please enter GitLab project ID to scan: " PROJECT_ID

    # Check if the provided PROJECT_ID is valid
    if [[ ! "$PROJECT_ID" =~ ^[1-9][0-9]{0,20}$ ]]; then
        printf "ERROR: Project ID must be a number\n"
        exit 1
    fi
}

[[ -z "${PROJECT_ID:-}" ]] && prompt_for_project_id

PROJECT_NAME=$(glab api "projects/$PROJECT_ID" | jq -r '.path')

REPOSITORY_IDS=$(mktemp)
TAGS=$(mktemp)
IMAGES=$(mktemp)

printf "Enumerating images in %s...\n" "$PROJECT_NAME"

glab api --paginate "projects/$PROJECT_ID/registry/repositories" | jq '.[].id' > "$REPOSITORY_IDS"

while read -r REPO_ID; do 
    glab api "projects/$PROJECT_ID/registry/repositories/$REPO_ID/tags" | jq -r '.[].name' > "$TAGS"
done < "$REPOSITORY_IDS"

paste -d , "$REPOSITORY_IDS" "$TAGS" > "$IMAGES"

while IFS=',' read -r REPO_ID TAG; do
    glab api "projects/$PROJECT_ID/registry/repositories/$REPO_ID/tags/$TAG" >> "${PROJECT_NAME}-registry.json"
done < "$IMAGES"
