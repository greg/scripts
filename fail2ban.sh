#!/usr/bin/env bash

# Enable strict mode
set -euo pipefail
IFS=$'\n\t'

# Error handling
trap 'echo "Error on line $LINENO. Exit code: $?" >&2' ERR

# Check if fail2ban is already installed
if command -v fail2ban-client >/dev/null 2>&1; then
    echo "fail2ban is already installed"
else
    echo "Installing fail2ban..."
    sudo apt-get update
    sudo apt-get install -y fail2ban
fi

# Configuration settings
JAIL_LOCAL="/etc/fail2ban/jail.local"
JAIL_CONF="/etc/fail2ban/jail.conf"

# Create jail.local if it doesn't exist
if [[ ! -f "$JAIL_LOCAL" ]]; then
    echo "Creating jail.local configuration file..."
    cp "$JAIL_CONF" "$JAIL_LOCAL"
fi

# Configure fail2ban settings
sudo echo "Configuring fail2ban settings..."
sudo sed -i 's/bantime  = 10m/bantime  = 1h/' "$JAIL_LOCAL"
sudo sed -i 's/findtime  = 10m/findtime  = 30m/' "$JAIL_LOCAL"
sudo sed -i 's/maxretry = 5/maxretry = 3/' "$JAIL_LOCAL"

# Enable and configure SSH protection
sudo echo "Configuring SSH protection..."
sudo sed -i 's/^#\[sshd\]/[sshd]/' "$JAIL_LOCAL"
sudo sed -i '/^\[sshd\]/a enabled = true' "$JAIL_LOCAL"
sudo echo "mode = aggressive" >> "$JAIL_LOCAL"

# Validate configuration
echo "Validating fail2ban configuration..."
if ! sudo fail2ban-client -t >/dev/null 2>&1; then
    echo "Error: fail2ban configuration test failed"
    exit 1
fi

# Restart fail2ban service
echo "Restarting fail2ban service..."
sudo systemctl restart fail2ban

# Check service status
if ! sudo systemctl is-active --quiet fail2ban; then
    echo "Error: fail2ban service failed to start"
    exit 1
fi

# Display status and configuration
echo "Checking fail2ban status..."
sudo systemctl status fail2ban

echo "Fail2ban has been installed and configured successfully"

# Display current jail status
echo "Current jail status:"
sudo fail2ban-client status sshd
