#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

## Update GitLab debian

sudo apt-get update
sudo gitlab-backup create
sudo apt-get upgrade gitlab-ee
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart


gitlab-update