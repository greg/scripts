#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

GIT_DIR="/home/greg/git"
GITLAB=gitlab
RUNNER=gitlab-runner
OMNIBUS=omnibus-gitlab
GITLAB_CHART=gitlab-charts

rg "$1" "$GIT_DIR/$GITLAB"
rg "$1" "$GIT_DIR/$RUNNER"
rg "$1" "$GIT_DIR/$OMNIBUS"
rg "$1" "$GIT_DIR/$GITLAB_CHART"
