#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Check if apt package manager is available
if ! command -v apt > /dev/null 2>&1; then
    echo "This script requires the apt package manager."
    exit 1
fi

# Function to update glab
update-glab() {
  command_exists() {
    command -v "$1" >/dev/null 2>&1
  }
  # Check if necessary commands are available
  for cmd in glab jq curl; do
    if ! command_exists "$cmd"; then
      echo "$cmd is not installed. Please install it first."
      exit 1
    fi
  done

# Function to check if system is Debian-based
check_debian_based() {
    if ! grep -Eiq 'debian|ubuntu|mint|kali' /etc/os-release; then
        log "Error: This script is only compatible with Debian-based distributions"
        exit 1
    fi
}

  # Get the current and latest glab versions
  current=$(glab version | awk '{print $4}')
  new=$(glab api projects/34675721/releases/permalink/latest | jq -r .tag_name | sed 's/v//g')
  echo "Current glab version: $current"
  echo "Latest glab version:  $new"
  # If the versions are not the same, update glab
  if [ "$current" != "$new" ]; then
    echo "Updating glab CLI from $current to $new"
    # Define the temporary file path
    temp_file="/tmp/glab_${new}_$(date +%s).deb"
    # Trap EXIT signal to ensure the temporary file is removed
    trap 'rm -f "$temp_file"' EXIT
    echo "Downloading glab $new to $temp_file"
    # Download the .deb file
    if ! curl -sL -o "$temp_file" "https://gitlab.com/gitlab-org/cli/-/releases/v$new/downloads/glab_${new}_linux_amd64.deb"; then
      echo "Failed to download glab $new"
      exit 1
    fi
    # Install the .deb file
    if ! sudo apt install -y "$temp_file"; then
      echo "Failed to install glab $new"
      exit 1
    fi
  else
    echo "glab version $current is already the latest version"
  fi
}

update-glab
