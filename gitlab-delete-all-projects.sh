#!/bin/bash

# Exit on error, use undefined variable as error
set -euo pipefail

# Step 1: Check if 'glab' is installed
if ! command -v glab &> /dev/null; then
    echo "glab is not installed, installing now..."
    if ! curl -L -s https://gitlab.com/greg/scripts/-/raw/master/glab-install.sh | bash; then
        echo "Failed to install glab. Please check your connection or the installation script."
        exit 1
    fi
fi

# Prompt user for GitLab hostname
read -rp "Enter your GitLab hostname (without http(s)://): " raw_gitlab_host
GITLAB_HOST=$(echo "$raw_gitlab_host" | sed -e 's,^http[s]\?://,,')
export GITLAB_HOST

# Prompt user for admin/root user's personal access token (with API scope)
read -rsp "Enter your admin/root user's personal access token: " GL_ADMIN_PAT
echo ""
export GL_ADMIN_PAT

# Authenticate as the admin/root user
if ! glab auth login --hostname "$GITLAB_HOST" -t "$GL_ADMIN_PAT"; then
    echo "Authentication failed. Check your hostname and personal access token."
    exit 1
fi

# get list of all projects
glab api --paginate /projects | jq -rc '.[]' | jq -rc '[.id, .web_url] | @csv' > projects.csv

while IFS=',' read -r project_id project_url; do
    echo "Deleting project ($project_url)"
    glab api /projects/$project_id -X DELETE
done < projects.csv

# delete all projects
# glab api --paginate /projects | jq -rc '.[].id' | xargs -n 1 -P 10 -I {} glab api /projects/{} -X DELETE