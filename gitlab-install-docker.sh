#!/usr/bin/env bash

##################
# WORK IN PROGRESS
exit 0
##################

# Enabling strict mode for better error handling
set -euo pipefail

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Check if Docker is installed
if ! command_exists docker; then
    echo "Docker is not installed. Would you like to install it? (yes/no)"
    read -r INSTALL_DOCKER
    case "${INSTALL_DOCKER,,}" in
        y|yes)
            curl -L get.docker.com | sh
            ;;
        *)
            echo "Docker is required to install GitLab. Exiting."
            exit 1
            ;;
    esac
fi

# Update packages
if command_exists apt-get; then
    sudo apt-get update && sudo apt-get upgrade -y
elif command_exists yum; then
    sudo yum update -y
elif command_exists dnf; then
    sudo dnf update -y
else
    echo "Unable to determine package manager. Please update your system manually."
fi

# Create GitLab home directory
sudo mkdir -p /srv/gitlab
export GITLAB_HOME=/srv/gitlab

# Prompt for GitLab external URL
while true; do
    echo "Please enter the external URL for GitLab (e.g., http://gitlab.example.com):"
    read -r EXTERNAL_URL
    if [[ "$EXTERNAL_URL" =~ ^https?://[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(/.*)?$ ]]; then
        break
    else
        echo "Invalid URL. Please include http:// or https:// and try again."
    fi
done

# Prompt for GitLab admin password
while true; do
    echo "Would you like to set the GitLab admin password? (yes/no)"
    read -r SET_PASSWORD
    case "${SET_PASSWORD,,}" in
        y|yes)
            while true; do
                echo "Please enter the admin password for GitLab (must be at least 8 characters long):"
                read -rs GITLAB_ADMIN_PASSWORD_INPUT
                if [[ ${#GITLAB_ADMIN_PASSWORD_INPUT} -ge 8 ]]; then
                    GITLAB_ADMIN_PASSWORD="$GITLAB_ADMIN_PASSWORD_INPUT"
                    break 2
                else
                    echo "Password must be at least 8 characters long."
                fi
            done
            ;;
        n|no)
            echo "Proceeding without setting an admin password."
            break
            ;;
        *)
            echo "Invalid input. Please enter yes or no."
            ;;
    esac
done

# Prompt for GitLab version
echo "Please enter the GitLab version you'd like to install (press Enter for latest):"
read -r GITLAB_VERSION
GITLAB_VERSION=${GITLAB_VERSION:-latest}

# Confirm installation
echo "Ready to install GitLab with the following configuration:"
echo "External URL: $EXTERNAL_URL"
echo "GitLab Version: $GITLAB_VERSION"
echo "GitLab Home: $GITLAB_HOME"
if [[ -n "${GITLAB_ADMIN_PASSWORD:-}" ]]; then
    echo "Admin password: Will be set to the provided password"
else
    echo "Admin password: Will be automatically generated"
fi

echo "Do you want to proceed with the installation? (yes/no)"
read -r CONFIRM_INSTALL
case "${CONFIRM_INSTALL,,}" in
    y|yes)
        # Run Docker command to install GitLab
        if [[ -n "${GITLAB_ADMIN_PASSWORD:-}" ]]; then
            sudo docker run --detach \
                --hostname "$(echo "$EXTERNAL_URL" | sed -e 's|^[^/]*//||' -e 's|/.*$||')" \
                --env GITLAB_OMNIBUS_CONFIG="external_url '$EXTERNAL_URL'; gitlab_rails['initial_root_password']='$GITLAB_ADMIN_PASSWORD';" \
                --publish 443:443 --publish 80:80 --publish 22:22 \
                --name gitlab \
                --restart always \
                --volume "$GITLAB_HOME/config:/etc/gitlab:Z" \
                --volume "$GITLAB_HOME/logs:/var/log/gitlab:Z" \
                --volume "$GITLAB_HOME/data:/var/opt/gitlab:Z" \
                --shm-size 256m \
                "gitlab/gitlab-ee:$GITLAB_VERSION"
        else
            sudo docker run --detach \
                --hostname "$(echo "$EXTERNAL_URL" | sed -e 's|^[^/]*//||' -e 's|/.*$||')" \
                --env GITLAB_OMNIBUS_CONFIG="external_url '$EXTERNAL_URL';" \
                --publish 443:443 --publish 80:80 --publish 22:22 \
                --name gitlab \
                --restart always \
                --volume "$GITLAB_HOME/config:/etc/gitlab:Z" \
                --volume "$GITLAB_HOME/logs:/var/log/gitlab:Z" \
                --volume "$GITLAB_HOME/data:/var/opt/gitlab:Z" \
                --shm-size 256m \
                "gitlab/gitlab-ee:$GITLAB_VERSION"
        fi

        echo "GitLab is now being installed. This may take a few minutes."
        echo "You can check the progress by running: sudo docker logs -f gitlab"
        
        # Wait for GitLab to be ready
        echo "Waiting for GitLab to be ready..."
        while ! sudo docker exec -it gitlab grep "gitlab Reconfigured!" /var/log/gitlab/reconfigure.log &> /dev/null; do
            echo -n "."
            sleep 10
        done
        echo "GitLab is ready!"

        # Print final message
        echo "GitLab has been successfully installed and is now available at $EXTERNAL_URL"
        if [[ -n "${GITLAB_ADMIN_PASSWORD:-}" ]]; then
            echo "The GitLab admin password has been set to the one you provided."
        else
            echo "The initial root password is:"
            sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
            echo "Please change this password immediately after logging in."
        fi
        ;;
    *)
        echo "Installation cancelled."
        exit 1
        ;;
esac
