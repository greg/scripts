#!/usr/bin/env bash

# This script checks for updates and installs the latest version of 'glab' if available.

set -euo pipefail

# Enable debug mode if DEBUG is set
[[ "${DEBUG:-}" ]] && set -x

BINDIR="${1:-/usr/bin}"

# Check installation permissions
if [[ ! -d "$BINDIR" ]] || [[ ! -w "$BINDIR" ]]; then
    echo "Error: Can't install to $BINDIR. Please check permissions or use sudo."
    exit 1
fi

# Determine OS and machine type
OS=$(uname -s)
MACHINE=$(uname -m)

case "$OS" in
    Linux|Darwin) OS=${OS/Darwin/macOS} ;;
    *) echo "Error: OS not supported"; exit 1 ;;
esac

[[ "$MACHINE" == "aarch64" ]] && MACHINE="arm64"

# Fetch current and latest 'glab' versions
INSTALLED_VERSION=$(glab --version | awk '{print $4}')
LATEST_VERSION=$(curl -sL https://gitlab.com/api/v4/projects/34675721/releases/permalink/latest | grep -o 'v[0-9\.]\+' | cut -c 2- | head -n 1)

if [[ "$INSTALLED_VERSION" == "$LATEST_VERSION" ]]; then
    echo "You have the latest version of glab (${INSTALLED_VERSION}) installed."
    exit 0
fi

echo "Updating glab from version $INSTALLED_VERSION to $LATEST_VERSION..."

# Create a temporary directory and ensure it's removed on script exit
TEMP_DIR=$(mktemp -d -t glab.XXXXXXXXXX)
trap 'rm -rf "$TEMP_DIR"' EXIT

# Download and extract the latest 'glab' version
DOWNLOAD_URL="https://gitlab.com/api/v4/projects/34675721/releases/v${LATEST_VERSION}/downloads/glab_${LATEST_VERSION}_${OS}_${MACHINE}.tar.gz"
if ! curl -sL "$DOWNLOAD_URL" | tar -C "$TEMP_DIR" -xz; then
    echo "Error: Failed to download or extract $DOWNLOAD_URL"
    exit 1
fi

# Install the binary
if ! install -m755 "${TEMP_DIR}/bin/glab" "${BINDIR}/glab"; then
    echo "Error: Failed to install glab to $BINDIR"
    exit 1
fi

echo "Successfully updated glab to version $LATEST_VERSION in $BINDIR."
