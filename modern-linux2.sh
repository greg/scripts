#!/usr/bin/env bash
# set -x # Uncomment for debugging

# Function to fetch the latest release from GitHub
get_latest_release() {
    curl -L -s -H 'Accept: application/json' "https://github.com/$1/releases/latest" | grep -oP '"tag_name":"\K(.*?)(?=")' | sed 's/v//'
}

# Create a unique temporary directory
TMP_DIR=$(mktemp -d -t software-XXXXXX)

# Declare an associative array with software repository as key and URL format as value
declare -A software_map=(
    ["sharkdp/bat"]="https://github.com/sharkdp/bat/releases/download/v%s/bat_%s_amd64.deb"
    ["ClementTsang/bottom"]="https://github.com/ClementTsang/bottom/releases/download/%s/bottom_%s_amd64.deb"
    ["muesli/duf"]="https://github.com/muesli/duf/releases/download/v%s/duf_%s_linux_amd64.deb"
    ["sharkdp/fd"]="https://github.com/sharkdp/fd/releases/download/v%s/fd_%s_amd64.deb"
    ["sharkdp/hyperfine"]="https://github.com/sharkdp/hyperfine/releases/download/v%s/hyperfine_%s_amd64.deb"
    ["Peltoche/lsd"]="https://github.com/Peltoche/lsd/releases/download/%s/lsd_%s_amd64.deb"
    ["peazip/PeaZip"]="https://github.com/peazip/PeaZip/releases/download/%s/peazip_%s.LINUX.GTK2-1_amd64.deb"
    ["BurntSushi/ripgrep"]="https://github.com/BurntSushi/ripgrep/releases/download/%s/ripgrep_%s_amd64.deb"
)

# Fetch and install deb packages
for repo in "${!software_map[@]}"; do
    version=$(get_latest_release "$repo")
    url=$(printf "${software_map[$repo]}" "$version" "$version")
    curl -L -s -o "$TMP_DIR/$(basename "$url")" "$url"
done

sudo apt install -y "$TMP_DIR"/*.deb

# Clean up
rm -rf "$TMP_DIR"
