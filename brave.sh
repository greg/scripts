#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# Function to check if Brave browser is installed
is_brave_installed() {
    if command -v brave-browser &> /dev/null; then
        return 0
    else
        return 1
    fi
}

# Check if Brave browser is already installed
if is_brave_installed; then
    echo "Brave browser is already installed."
    exit 0
fi

echo "Starting installation process..."

# Install Brave browser
echo "Downloading Brave browser keyring..."
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "Adding Brave browser repository..."
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

echo "Updating package list..."
sudo apt update

echo "Installing Brave browser..."
sudo apt install -y brave-browser

echo "Brave browser has been successfully installed!"
