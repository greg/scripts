#!/usr/bin/env bash
# set -x # Uncomment for debugging

# Function to get the latest release from GitHub
get_latest_release() {
    curl -L -s -H 'Accept: application/json' "https://github.com/$1/releases/latest" | grep -oP '"tag_name":"\K(.*?)(?=")' | sed 's/v//'
}

GRYPE_CURRENT_VERSION=$(grype version | grep -oP '^Version:\s*\K.*')
GRYPE_LATEST_VERSION=$(get_latest_release "anchore/grype")

TRIVY_CURRENT_VERSION=$(trivy --version | grep -oP '^Version:\s*\K.*')
TRIVY_LATEST_VERSION=$(get_latest_release "aquasecurity/trivy")

TRUFFLEHOG_CURRENT_VERSION=$(trufflehog --version | awk '{print $2}')
TRUFFLEHOG_LATEST_VERSION=$(get_latest_release "trufflesecurity/trufflehog")

# Create a unique temporary directory
TMP_DIR=$(mktemp -d -t software-XXXXXX)

# Fetch and install deb packages
curl -LO "$TMP_DIR/grype_${GRYPE_LATEST_VERSION}_linux_amd64.deb" "https://github.com/anchore/grype/releases/download/v$GRYPE_LATEST_VERSION/grype_${GRYPE_LATEST_VERSION}_linux_amd64.deb"
curl -LO "$TMP_DIR/trivy_${TRIVY_LATEST_VERSION}_Linux-64bit.deb" "https://github.com/aquasecurity/trivy/releases/download/v$TRIVY_LATEST_VERSION/trivy_${TRIVY_LATEST_VERSION}_Linux-64bit.deb"
curl -LO "$TMP_DIR/trufflehog_${TRUFFLEHOG_LATEST_VERSION}_Linux-64bit.deb"
sudo apt install "$TMP_DIR"/*.deb

# Clean up
rm -rf "$TMP_DIR"
