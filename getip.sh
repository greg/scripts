#!/usr/bin/env bash

# get your external IP address (IPv4)

function getip {
  if [ -x "$(which curl)" ] ; then
    curl icanhazip.com
  else
    echo "curl not installed, please install curl to use this script"
  fi
}

getip