#!/usr/bin/env bash
set -u
set -e
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o pipefail
IFS=$'\n\t'

git clone git@gitlab.com:gitlab-org/gitlab.git
git clone git@gitlab.com:gitlab-org/omnibus-gitlab.git
git clone git@gitlab.com:gitlab-org/charts/gitlab-runner.git
git clone git@gitlab.com:gitlab-org/charts/gitlab.git
git clone git@gitlab.com:gitlab-org/gitaly.git
git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
git clone git@gitlab.com:gitlab-com/marketing/digital-experience/buyer-experience.git
git clone git@gitlab.com:gitlab-org/os-license-checker.git
