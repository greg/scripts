#!/bin/bash
set -euo pipefail

# Check if apt package manager is available
if ! command -v apt > /dev/null 2>&1; then
    echo "This script requires the apt package manager."
    exit 1
fi

# Function to check if system is Debian-based
check_debian_based() {
    if ! grep -Eiq 'debian|ubuntu|mint|kali' /etc/os-release; then
        log "Error: This script is only compatible with Debian-based distributions"
        exit 1
    fi
}

command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if necessary commands are available
for cmd in jq curl; do
  if ! command_exists "$cmd"; then
    echo "$cmd is not installed. Please install it first."
    exit 1
  fi
done

# Function to update glab
install-glab() {
  # Get the latest glab versions
  latest=$(curl -sL https://gitlab.com/api/v4/projects/34675721/releases/permalink/latest | grep --only-matching 'v[0-9\.]\+' | cut -c 2- | head -n 1)
  # Define the temporary file path
  temp_file="/tmp/glab_${latest}_$(date +%s).deb"
  echo "Downloading glab $latest to $temp_file"
  # Download the .deb file
  curl -sL -o "$temp_file" "https://gitlab.com/gitlab-org/cli/-/releases/v$latest/downloads/glab_${latest}_linux_amd64.deb"
  sudo apt install -y "$temp_file"
}

install-glab
