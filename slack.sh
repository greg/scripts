#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

curl -s https://packagecloud.io/install/repositories/slacktechnologies/slack/script.deb.sh | sudo bash
sudo sed -i 's/bookworm/jessie/g' /etc/apt/sources.list.d/slacktechnologies-slack.list
sudo apt-get update
sudo apt-get install -y slack-desktop