#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# Function to install Firefox from Mozilla's repository
install_firefox() {
    echo "Adding Mozilla's APT repository and installing Firefox..."
    sudo install -d -m 0755 /etc/apt/keyrings || handle_error "Failed to create keyrings directory"
    wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- | sudo tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null || handle_error "Failed to download Mozilla signing key"
    echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" | sudo tee /etc/apt/sources.list.d/mozilla.list > /dev/null || handle_error "Failed to add Mozilla APT repository"
    echo "Package: *" | sudo tee /etc/apt/preferences.d/mozilla > /dev/null
    echo "Pin: origin packages.mozilla.org" | sudo tee -a /etc/apt/preferences.d/mozilla > /dev/null
    echo "Pin-Priority: 1000" | sudo tee -a /etc/apt/preferences.d/mozilla > /dev/null
    sudo apt-get update && sudo apt-get install -y firefox || handle_error "Failed to install Firefox"
}

install_firefox