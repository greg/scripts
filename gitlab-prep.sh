#!/usr/bin/env bash
set -ex
sudo apt-get update &&
sudo apt-get install -y curl openssh-server ca-certificates &&
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash