#!/usr/bin/env bash

# Define the target file
TARGET_FILE="$HOME/.dir_colors"

# Download the Nord dir_colors file
curl -L -o "$TARGET_FILE" https://raw.githubusercontent.com/arcticicestudio/nord-dircolors/develop/src/dir_colors

# Check if the file exists and is readable, then apply the colors
if [[ -r "$TARGET_FILE" ]]; then
  eval "$(dircolors "$TARGET_FILE")"
else
  echo "File $TARGET_FILE not found or not readable, skipping color application."
fi
