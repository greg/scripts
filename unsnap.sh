#!/usr/bin/env bash
set -euo pipefail

remove_snap_packages() {
    echo "Removing all Snap packages..."
    snap list | awk 'NR > 1 {print $1}' | xargs -I {} sudo snap remove {} || true
}

remove_snapd() {
    echo "Stopping, disabling, and masking snapd service..."
    sudo systemctl stop snapd || true
    sudo systemctl disable snapd || true
    sudo systemctl mask snapd || true
    
    echo "Removing Snapd and cleaning up..."
    sudo apt autoremove --purge snapd -y || true
    sudo rm -rf ~/snap /snap /var/snap /var/lib/snapd /var/cache/snapd/
}

create_preference_file() {
    sudo apt-mark hold snapd
    echo "Creating preference file to prevent Snap from being reinstalled..."
    cat << EOF | sudo tee /etc/apt/preferences.d/nosnap.pref > /dev/null
Package: snapd
Pin: release a=*
Pin-Priority: -10
EOF
}

echo "Starting Snap removal process..."

remove_snapd
while snap list 2>/dev/null | awk 'NR > 1'; do
    remove_snap_packages
    echo "Waiting for Snap packages to be fully removed..."
    sleep 5
done

create_preference_file

echo "Snap removal process completed."
