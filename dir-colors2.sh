#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e
# Treat unset variables as an error and exit
set -u
# Make script fail if any command in a pipeline fails
set -o pipefail

# Constants
DIRCOLORS_CONFIG="$HOME/.dir_colors"
DOWNLOAD_URL="https://raw.githubusercontent.com/arcticicestudio/nord-dircolors/develop/src/dir_colors"

# Check if required commands (curl and dircolors) are available on the system
check_commands() {
  for cmd in curl dircolors; do
    if ! command -v "$cmd" &>/dev/null; then
      echo "Error: $cmd is not available. Please install it first."
      exit 1
    fi
  done
}

# Cleanup in case of any errors, remove the target file
cleanup() {
  if [[ -f "$DIRCOLORS_CONFIG" ]]; then
    rm -f "$DIRCOLORS_CONFIG"
  fi
}
# Register the cleanup function to be called on exit
trap cleanup EXIT

# Download the Nord dir_colors file
download_colors() {
  echo "Downloading dir_colors..."
  if ! curl -L -o "$DIRCOLORS_CONFIG" "$DOWNLOAD_URL"; then
    echo "Error: Failed to download from $DOWNLOAD_URL"
    exit 1
  fi
}

# Apply the Nord dir_colors configuration
apply_colors() {
  if [[ -r "$DIRCOLORS_CONFIG" ]]; then
    echo "Applying colors..."
    # Source the configuration using dircolors for safety
    source <(dircolors "$DIRCOLORS_CONFIG")
  else
    echo "Error: File $DIRCOLORS_CONFIG not found or not readable. Aborting color application."
    exit 1
  fi
}

# Main execution flow of the script
main() {
  check_commands       # Ensure required commands are present
  download_colors      # Download the color configuration
  apply_colors         # Apply the color configuration
}

# Start the script by calling the main function
main
