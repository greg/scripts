#!/usr/bin/env bash
set -u
set -e
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o pipefail
IFS=$'\n\t'

USERNAME="myersg86"
EMAIL="myersg86@gmail.com"
KEY="0AE817DC21939433"

git config user.name "$USERNAME"
git config user.email "$EMAIL"
git config user.signingkey "$KEY"
git config commit.gpgsign true
