#!/usr/bin/env bash
set -x

GRYPE_CURRENT_VERSION=$(grype version | grep -E '^Version:' | awk '{print $2}')
GRYPE_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/anchore/grype/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | sed -e 's/v//g')
TRIVY_CURRENT_VERSION=$(trivy --version | grep -E '^Version:' | awk '{print $2}')
TRIVY_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/aquasecurity/trivy/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | sed -e 's/v//g')
TRUFFLEHOG_CURRENT_VERSION=$(trufflehog --version | awk '{print $2}')
TRUFFLEHOG_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/trufflesecurity/trufflehog/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | sed -e 's/v//g')

## install deb packages

mkdir /tmp/software
cd /tmp/software
curl -LO "https://github.com/anchore/grype/releases/download/v$GRYPE_LATEST_VERSION/grype_${GRYPE_LATEST_VERSION}_linux_amd64.deb"
curl -LO "https://github.com/aquasecurity/trivy/releases/download/v$TRIVY_LATEST_VERSION/trivy_${TRIVY_LATEST_VERSION}_Linux-64bit.deb"
sudo apt install ./*.deb
rm -rf /tmp/software/*.deb

## install binaries

