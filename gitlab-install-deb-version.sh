#!/usr/bin/env bash

# Enabling strict mode for better error handling
set -euo pipefail

# Ensure the script is running with root privileges
# if [[ $(id -u) -ne 0 ]]; then
#     echo "This script must be run as root or with sudo."
#     exit 1
# fi

# Function to check if system is Debian-based
check_debian_based() {
    if ! grep -Eiq 'debian|ubuntu|mint|kali' /etc/os-release; then
        log "Error: This script is only compatible with Debian-based distributions"
        exit 1
    fi
}

# Update and install necessary packages
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl

# Prompt for installation of postfix and mailutils
while true; do
    echo "Do you want to install postfix and mailutils? (y/n)"
    read -r POSTFIX_INSTALL_RESPONSE
    case "$POSTFIX_INSTALL_RESPONSE" in
        [Yy]|[Yy][Ee][Ss])
            sudo apt-get install -y postfix mailutils
            break
            ;;
        [Nn]|[Nn][Oo])
            echo "Not installing postfix and mailutils."
            break
            ;;
        *)
            echo "Invalid response. Please enter 'yes' or 'no'."
            ;;
    esac
done

# Interactive setup for GitLab root password
while true; do
    echo "Do you want to set a root password for GitLab? (yes/no)"
    read -r SET_PASSWORD

    case "$SET_PASSWORD" in
        [Yy]|[Yy][Ee][Ss])
            while true; do
                echo "Please enter the root password for GitLab (must be at least 12 characters long):"
                read -sr GITLAB_ADMIN_PASSWORD_INPUT
                if [[ ${#GITLAB_ADMIN_PASSWORD_INPUT} -ge 12 ]]; then
                    export GITLAB_ADMIN_PASSWORD="$GITLAB_ADMIN_PASSWORD_INPUT"
                    break 2  # Break out of both loops when password is set
                else
                    echo "Password must be at least 12 characters long."
                fi
            done
            ;;
        [Nn]|[Nn][Oo])
            echo "Proceeding without setting a root password."
            break
            ;;
        *)
            echo "Invalid input. Please enter 'yes' or 'no'."
            ;;
    esac
done

# Prompt for the external URL and validate the input
while true; do
    echo "Please enter the external URL for GitLab (including http:// or https://):"
    read -r EXTERNAL_URL_INPUT
    if [[ "$EXTERNAL_URL_INPUT" =~ ^https?://[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(/.*)?$ ]]; then
        EXTERNAL_URL="$EXTERNAL_URL_INPUT"
        break
    else
        echo "Invalid URL. Please include http:// or https:// and try again."
    fi
done

# Add GitLab's official repository via script
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

# Prompt for specific GitLab version
while true; do
    echo "Would you like to install a specific version of GitLab? (y/n)"
    read -r SPECIFIC_VERSION
    case "$SPECIFIC_VERSION" in
        [Yy]|[Yy][Ee][Ss])
            echo "Please enter the version (e.g., 17.7.1):"
            read -r VERSION
            if [[ "$VERSION" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
                INSTALL_CMD="sudo EXTERNAL_URL=\"$EXTERNAL_URL\" apt-get install -y gitlab-ee=$VERSION-ee.0"
                break
            else
                echo "Invalid version format. Please use format: major.minor.patch (e.g., 17.7.1)"
            fi
            ;;
        [Nn]|[Nn][Oo]|[Ll][Aa][Tt][Ee][Ss][Tt]|"")
            INSTALL_CMD="sudo EXTERNAL_URL=\"$EXTERNAL_URL\" apt-get install -y gitlab-ee"
            break
            ;;
        *)
            echo "Invalid response. Please enter 'y' for specific version or 'n' for latest."
            ;;
    esac
done

# Install GitLab with the appropriate command
if [[ -v GITLAB_ADMIN_PASSWORD ]]; then
    eval "GITLAB_ROOT_PASSWORD=\"$GITLAB_ADMIN_PASSWORD\" $INSTALL_CMD"
else
    eval "$INSTALL_CMD"
fi

# Display final status or message
echo "GitLab installation completed. GitLab is now available at $EXTERNAL_URL"
if [[ -v GITLAB_ADMIN_PASSWORD ]]; then
    echo "GitLab root password has been set."
else
    if [[ -f /etc/gitlab/initial_root_password ]]; then
        echo "No root password has been manually set for GitLab."
        echo "The root user password is $(cat /etc/gitlab/initial_root_password)"
    else
        echo "No root password has been set and no initial password file is found."
    fi
fi
