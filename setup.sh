#!/usr/bin/env bash

chmod +x *.sh
./dotfiles.sh
./tzdata.sh
./support-utils.sh
./modern-linux.sh
./dir-colors.sh
./docker.sh
./tldr.sh
