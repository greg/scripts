#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e

# Exit if any part of a pipeline fails
set -o pipefail

# Treat unset variables as an error
set -u

# TLDR script URL
TLDR_URL="https://raw.githubusercontent.com/raylee/tldr/master/tldr"
INSTALL_DIR="/usr/local/bin"
TLDR_PATH="${INSTALL_DIR}/tldr"

# Function to log messages
log() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

# Check if script is run with sudo
if [[ $EUID -ne 0 ]]; then
   log "This script must be run with sudo privileges"
   exit 1
fi

# Download TLDR script
log "Downloading TLDR script from ${TLDR_URL}"
if ! curl -sSf -o "${TLDR_PATH}" "${TLDR_URL}"; then
    log "Error: Failed to download TLDR script"
    exit 1
fi

# Make TLDR script executable
log "Making TLDR script executable"
chmod +x "${TLDR_PATH}"

log "TLDR script has been successfully installed to ${TLDR_PATH}"
log "You can now use TLDR by typing 'tldr' followed by a command name"