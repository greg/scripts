#!/usr/bin/env bash
set -uo pipefail
IFS=$'\n\t'

# Function to pull Docker images with error handling
pull_image() {
    echo "Pulling $1..."
    if ! docker pull "$1"; then
        echo "Failed to pull $1" >&2
    fi
}

# Function to pull images in parallel
pull_images_parallel() {
    local images=("$@")
    for image in "${images[@]}"; do
        pull_image "$image" &
    done
    wait
}

echo "Starting Docker image pull process..."

# Base images
base_images=(
    "alpine"
    "alpine:3.20"
    "almalinux:9"
    "almalinux:8"
    "busybox"
    "debian:bullseye"
    "debian:bullseye-slim"
    "debian:trixie"
    "ubuntu:20.04"
    "ubuntu:22.04"
    "ubuntu:24.04"
)

# Development images
dev_images=(
    "curlimages/curl"
    "docker"
    "docker:dind"
    "golang:latest"
    "golang:alpine"
    "golang:bookworm"
    "nginx"
    "node:current"
    "node:lts"
    "node:alpine"
    "openbao/openbao"
    "python:alpine"
    "python:bookworm" 
    "python:bookworm-slim"
    "ruby:latest" 
    "registry"
)

# GitLab images
gitlab_images=(
    "gitlab/gitlab-ee:latest"
    "gitlab/gitlab-runner:latest"
)

# Security scanner images
scanner_images=(
    "registry.gitlab.com/security-products/api-security:5"
    "registry.gitlab.com/security-products/container-scanning:7"
    "registry.gitlab.com/security-products/dast:5"
    "registry.gitlab.com/security-products/flawfinder:5"
    "registry.gitlab.com/security-products/gemnasium:5"
    "registry.gitlab.com/security-products/gemnasium-maven:5"
    "registry.gitlab.com/security-products/gemnasium-python:5"
    "registry.gitlab.com/security-products/kubesec:4"
    "registry.gitlab.com/security-products/kics:5"
    "registry.gitlab.com/security-products/secrets:6"
    "registry.gitlab.com/security-products/semgrep:5"
    "registry.gitlab.com/security-products/spotbugs:5"
)

# Security tool images
security_images=(
    "bearer/bearer" 
    "presidentbeef/brakeman" 
    "bridgecrew/checkov"
    "clamav/clamav" 
    "owasp/dependency-check:latest"
    "ghcr.io/owasp-dep-scan/dep-scan"
    "goodwithtech/dockle"
    "zricethezav/gitleaks:latest"
    "anchore/grype:latest"
    "ghcr.io/datadog/guarddog"
    "hadolint/hadolint:latest-debian"
    "hadolint/hadolint:latest-alpine"
    "oxsecurity/megalinter:v8"
    "opensecurity/mobsfscan"
    "ghcr.io/google/osv-scanner:latest"
    "gcr.io/openssf/scorecard:stable"
    "returntocorp/semgrep"
    "aquasec/trivy"
    "trufflesecurity/trufflehog:latest"
)

echo "Pulling base images..."
pull_images_parallel "${base_images[@]}"

echo "Pulling development images..."
pull_images_parallel "${dev_images[@]}"

echo "Pulling GitLab images..."
pull_images_parallel "${gitlab_images[@]}"

echo "Pulling security scanner images..."
pull_images_parallel "${scanner_images[@]}"

echo "Pulling security tool images..."
pull_images_parallel "${security_images[@]}"

echo "Docker image pull process completed."
