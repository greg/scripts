#!/usr/bin/env bash

# Enabling strict mode for better error handling
set -euo pipefail

# Improved error handling
trap 'echo "An error occurred. Exiting..."; exit 1' ERR

# Function to check if the system is supported
check_system() {
    if [ -f /etc/os-release ]; then
        . /etc/os-release
        if [[ "$ID" =~ ^(rhel|almalinux|rocky|centos|ol)$ && "${VERSION_ID%%.*}" =~ ^(7|8|9)$ ]]; then
            echo "Supported system detected: $PRETTY_NAME"
        else
            echo "Error: Unsupported system. This script is for Enterprise Linux 7/8/9 and compatible."
            exit 1
        fi
    else
        echo "Error: Unable to determine the operating system."
        exit 1
    fi
}

# Determine package manager
if command -v dnf &> /dev/null; then
    PKG_MANAGER="dnf"
elif command -v yum &> /dev/null; then
    PKG_MANAGER="yum"
else
    echo "Error: Neither dnf nor yum package manager found."
    exit 1
fi

# Function to install packages
install_packages() {
    local packages=("$@")
    sudo $PKG_MANAGER install -y "${packages[@]}"
}

# Check if the system is supported
check_system

# Update system and install necessary packages
echo "Updating system and installing necessary packages..."
sudo $PKG_MANAGER update -y
install_packages curl policycoreutils openssh-server perl

# Configure and start sshd if not already running
if ! systemctl is-active --quiet sshd; then
    echo "Configuring and starting sshd..."
    sudo systemctl enable sshd
    sudo systemctl start sshd
else
    echo "sshd is already running. Continuing..."
fi

# Configure firewall
if systemctl is-active --quiet firewalld; then
    echo "Configuring firewall..."
    sudo firewall-cmd --permanent --add-service=http
    sudo firewall-cmd --permanent --add-service=https
    sudo systemctl reload firewalld
else
    echo "Firewalld is not running. Skipping firewall configuration."
fi

# Prompt for installation of postfix
while true; do
    read -rp "Do you want to install postfix? (y/n): " POSTFIX_INSTALL_RESPONSE
    case "$POSTFIX_INSTALL_RESPONSE" in
        [Yy]|[Yy][Ee][Ss])
            echo "Installing and configuring postfix..."
            install_packages postfix
            sudo systemctl enable postfix
            sudo systemctl start postfix
            break
            ;;
        [Nn]|[Nn][Oo])
            echo "Skipping postfix installation."
            break
            ;;
        *)
            echo "Invalid response. Please enter 'yes' or 'no'."
            ;;
    esac
done

# Interactive setup for GitLab root password
while true; do
    read -rp "Do you want to set a root password for GitLab? (yes/no): " SET_PASSWORD
    case "$SET_PASSWORD" in
        [Yy]|[Yy][Ee][Ss])
            while true; do
                read -rsp "Please enter the root password for GitLab (must be at least 12 characters long): " GITLAB_ADMIN_PASSWORD_INPUT
                echo
                if [[ ${#GITLAB_ADMIN_PASSWORD_INPUT} -ge 12 ]]; then
                    export GITLAB_ROOT_PASSWORD="$GITLAB_ADMIN_PASSWORD_INPUT"
                    break 2
                else
                    echo "Password must be at least 12 characters long."
                fi
            done
            ;;
        [Nn]|[Nn][Oo])
            echo "Proceeding without setting a root password."
            break
            ;;
        *)
            echo "Invalid input. Please enter 'yes' or 'no'."
            ;;
    esac
done

# Prompt for the external URL and validate the input
while true; do
    read -rp "Please enter the external URL for GitLab (including http:// or https://): " EXTERNAL_URL_INPUT
    if [[ "$EXTERNAL_URL_INPUT" =~ ^https?://[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}(/.*)?$ ]]; then
        EXTERNAL_URL="$EXTERNAL_URL_INPUT"
        break
    else
        echo "Invalid URL. Please include http:// or https:// and try again."
    fi
done

# Prompt for GitLab edition
while true; do
    read -rp "Do you want to install GitLab Community Edition (CE) or Enterprise Edition (EE)? (ce/ee): " GITLAB_EDITION
    case "$GITLAB_EDITION" in
        [Cc][Ee])
            GITLAB_PACKAGE="gitlab/gitlab-ce"
            break
            ;;
        [Ee][Ee])
            GITLAB_PACKAGE="gitlab/gitlab-ee"
            break
            ;;
        *)
            echo "Invalid input. Please enter 'ce' for Community Edition or 'ee' for Enterprise Edition."
            ;;
    esac
done

# Add GitLab's official repository and install GitLab
echo "Adding GitLab repository and installing GitLab..."
curl "https://packages.gitlab.com/install/repositories/${GITLAB_PACKAGE}/script.rpm.sh" | sudo bash
if [[ -v GITLAB_ROOT_PASSWORD ]]; then
    sudo EXTERNAL_URL="$EXTERNAL_URL" GITLAB_ROOT_PASSWORD="$GITLAB_ROOT_PASSWORD" $PKG_MANAGER install -y "${GITLAB_PACKAGE##*/}"
else
    sudo EXTERNAL_URL="$EXTERNAL_URL" $PKG_MANAGER install -y "${GITLAB_PACKAGE##*/}"
fi

# Display final status or message
echo "GitLab installation completed. GitLab is now available at $EXTERNAL_URL"
if [[ -v GITLAB_ROOT_PASSWORD ]]; then
    echo "GitLab root password has been set."
else
    if [[ -f /etc/gitlab/initial_root_password ]]; then
        echo "No root password has been manually set for GitLab."
        echo "The initial root user password is $(cat /etc/gitlab/initial_root_password)"
        echo "The initial root password is stored in /etc/gitlab/initial_root_password"
        echo "Please change this password as soon as possible!"
    else
        echo "No root password has been set and no initial password file is found."
        echo "Please check GitLab documentation for password reset instructions."
    fi
fi

echo "Installation complete. You may need to configure GitLab further based on your specific needs."
