#!/usr/bin/env bash

# Script to install and configure Syncthing on Debian-based systems
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# Function to check if system is Debian-based
check_debian_based() {
    if ! grep -Eiq 'debian|ubuntu|mint|kali' /etc/os-release; then
        log "Error: This script is only compatible with Debian-based distributions"
        exit 1
    fi
}

sudo mkdir -p /etc/apt/keyrings
sudo curl -L -o /etc/apt/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt-get update
sudo apt-get install syncthing
