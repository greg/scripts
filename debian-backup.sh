#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

BACKUP_DESTINATION="/media/backup"

# This script is intended to be run as root.
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root." >&2
   exit 1
fi

rsync -r /etc $BACKUP_DESTINATION
rsync -r /var/lib/dpkg $BACKUP_DESTINATION
rsync /etc/apt/sources.list $BACKUP_DESTINATION
rsync -r /etc/apt/sources.list.d $BACKUP_DESTINATION
rsync -r /var/lib/apt/extended_states $BACKUP_DESTINATION
dpkg --get-selections '*' > $BACKUP_DESTINATION/Package.list
# rsync /home $BACKUP_DESTINATION
