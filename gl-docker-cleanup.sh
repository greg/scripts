#!/usr/bin/env bash

# This script stops and removes the GitLab Docker container and cleans up associated directories.

# Exit immediately if a command exits with a non-zero status
set -euo pipefail

# Print a message and exit if an error occurs during script execution
handle_error() {
    echo "Error on line $LINENO. Exit code: $?" >&2
    exit 1
}
trap handle_error ERR

# Docker container and associated directories
CONTAINER_NAME="gitlab"
CONFIG_DIR="/srv/config"
LOGS_DIR="/srv/logs"
DATA_DIR="/srv/data"

# Stop and remove the Docker container
sudo docker stop "$CONTAINER_NAME"
sudo docker rm "$CONTAINER_NAME"

# Remove associated directories
sudo rm -rf "$CONFIG_DIR" "$LOGS_DIR" "$DATA_DIR"
