#!/usr/bin/env bash
set -x

# BAT_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/sharkdp/bat/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/'| sed -e 's/v//g')
BOTTOM_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/ClementTsang/bottom/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' )
DUF_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/muesli/duf/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | sed -e 's/v//g')
# FD_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/sharkdp/fd/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/'| sed -e 's/v//g')
# HYPERFINE_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/sharkdp/hyperfine/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/'| sed -e 's/v//g')
# LSD_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/Peltoche/lsd/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
PEAZIP_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/peazip/PeaZip/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
# RIPGREP_LATEST_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/BurntSushi/ripgrep/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')

## install deb packages

mkdir /tmp/software
cd /tmp/software
# curl -LO "https://github.com/sharkdp/bat/releases/download/v$BAT_LATEST_VERSION/bat_${BAT_LATEST_VERSION}_amd64.deb"
curl -LO "https://github.com/ClementTsang/bottom/releases/download/$BOTTOM_LATEST_VERSION/bottom_${BOTTOM_LATEST_VERSION}_amd64.deb"
curl -LO "https://github.com/muesli/duf/releases/download/v$DUF_LATEST_VERSION/duf_${DUF_LATEST_VERSION}_linux_amd64.deb"
# curl -LO "https://github.com/sharkdp/fd/releases/download/v$FD_LATEST_VERSION/fd_${FD_LATEST_VERSION}_amd64.deb"
# curl -LO "https://github.com/sharkdp/hyperfine/releases/download/v$HYPERFINE_LATEST_VERSION/hyperfine_${HYPERFINE_LATEST_VERSION}_amd64.deb"
# curl -LO "https://github.com/Peltoche/lsd/releases/download/$LSD_LATEST_VERSION/lsd_${LSD_LATEST_VERSION}_amd64.deb"
curl -LO "https://github.com/peazip/PeaZip/releases/download/$PEAZIP_LATEST_VERSION/peazip_$PEAZIP_LATEST_VERSION.LINUX.GTK2-1_amd64.deb"
# curl -LO "https://github.com/BurntSushi/ripgrep/releases/download/$RIPGREP_LATEST_VERSION/ripgrep_${RIPGREP_LATEST_VERSION}_amd64.deb"
sudo apt install -y /tmp/software/*.deb
rm -rf /tmp/software/*.deb
