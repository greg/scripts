#!/usr/bin/env bash
set -x

SSG_LATEST=$(curl -L -s -H 'Accept: application/json' https://github.com/ComplianceAsCode/content/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/'| sed -e 's/v//g')

curl -LO "https://github.com/ComplianceAsCode/content/releases/download/v$SSG_LATEST/scap-security-guide-$SSG_LATEST.zip"