#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

sudo apt install -y \
   apt-transport-https \
   bash-completion \
   bat \
   busybox \
   ca-certificates \
   curl \
   cpufrequtils \
   duf \
   exa \
   fakeroot \
   fd-find \
   fzf \
   git \
   gnupg2 \
   htop \
   jq \
   lsb-release \
   mc \
   ncdu \
   openssl \
   parallel \
   pass \
   ripgrep \
   rsync \
   software-properties-common \
   tmux \
   tree \
   unzip \
   wget