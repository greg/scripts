#!/usr/bin/env bash

# right now, this is for Ubuntu 20.04

sudo apt update &&
sudo apt install -y bash-completion curl git jq htop httpie lnav lsof mc nmap net-tools ranger strace tcpdump tmux tree vim whois
