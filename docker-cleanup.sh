#!/usr/bin/env bash

# Docker Cleanup Script
# This script removes all Docker containers, images, and performs a system prune.

set -euo pipefail

echo "This script will stop and remove all Docker containers and images, and perform a system prune."
echo "Are you sure you want to proceed? (yes/y)"
read response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    # Stop all running Docker containers
    echo "Stopping all running Docker containers..."
    sudo docker stop $(sudo docker ps -q) 2>/dev/null || true

    # Remove all Docker containers
    echo "Removing all Docker containers..."
    sudo docker rm -f $(sudo docker ps -aq) 2>/dev/null || true

    # Remove all Docker images
    echo "Removing all Docker images..."
    sudo docker rmi -f $(sudo docker images -q) 2>/dev/null || true

    # Perform Docker system prune
    echo "Performing Docker system prune..."
    sudo docker system prune -af --volumes

    echo "Docker cleanup completed."
else
    echo "Operation cancelled."
    exit 0
fi
