#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Print commands and their arguments as they are executed
set -x

# Add Golang plugin to ASDF
asdf plugin-add golang https://github.com/kennyp/asdf-golang.git

# Install latest version of Golang
asdf install golang latest

# Reshim Golang
asdf reshim golang

echo "Golang has been installed and reshimmed successfully using ASDF."

# Optionally, you can set the installed version as global
# Uncomment the following line if you want to set it as global
# asdf global golang $(asdf latest golang)

echo "You may want to set the installed version as global using:"
echo "asdf global golang \$(asdf latest golang)"