#!/usr/bin/env bash

sudo apt install curl coreutils dirmngr gawk git gpg
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch release-v0.9.1
echo "make sure the following is added to .bashrc: . $HOME/.asdf/asdf.sh"
asdf plugin-add golang https://github.com/kennyp/asdf-golang.git
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
apt-get install autoconf patch build-essential rustc libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libgmp-dev libncurses5-dev libffi-dev libgdbm6 libgdbm-dev libdb-dev uuid-dev
asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git
# asdf install golang latest
# asdf install ruby latest
# asdf plugin-add postgres
# asdf plugin-add java https://github.com/halcyon/asdf-java.git
# asdf plugin-add rust https://github.com/code-lever/asdf-rust.git