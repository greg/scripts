#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'
DEBIAN_FRONTEND=noninteractive

# Ensure the script is run as sudo
if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as with sudo."
    exit 1
fi

# Check if either wget or curl is installed
if command -v wget &> /dev/null; then
    downloader="wget -O-"
elif command -v curl &> /dev/null; then
    downloader="curl -sL"
else
    echo "Either wget or curl is required but neither is installed. Exiting."
    exit 1
fi

# Check if the system is Debian-based
if ! grep -Ei 'debian|ubuntu|mint|kali|knoppix|mx|pureos|raspbian|siduction|sparkylinux|solydxk|steamos|devuan' /etc/os-release > /dev/null; then
    echo "This script is only compatible with Debian-based distributions."
    exit 1
fi

# Update and install gpg
if ! command -v gpg &> /dev/null; then
    apt-get update
    apt-get install -y gpg
fi

# Download and set up HashiCorp GPG key if it doesn't exist
KEYRING_PATH="/usr/share/keyrings/hashicorp-archive-keyring.gpg"
if [[ ! -f $KEYRING_PATH ]]; then
    $downloader https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o $KEYRING_PATH
fi

echo "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRING_PATH] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list

# Update and install Vault
apt-get update && apt-get install -y vault
