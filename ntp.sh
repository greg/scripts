#!/usr/bin/env bash

# Set bash options for error handling and safer execution
set -euo pipefail
IFS=$'\n\t'

# Trap function to display error messages
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Function to check if a command was successful
check_command() {
    if [ $? -ne 0 ]; then
        echo "Error: $1 failed" >&2
        exit 1
    fi
}

# NTP server to use
NTP_SERVER="time.nist.gov"

# Check if NTP is already installed
if ! command -v ntpdate &> /dev/null; then
    echo "Installing NTP..."
    sudo apt install -y ntp ntpdate
    check_command "NTP installation"
else
    echo "NTP is already installed."
fi

# Stop NTP service
echo "Stopping NTP service..."
sudo systemctl stop ntp
check_command "Stopping NTP service"

# Update time from NTP server
echo "Updating time from $NTP_SERVER..."
sudo ntpdate -s "$NTP_SERVER"
check_command "Updating time"

# Start NTP service
echo "Starting NTP service..."
sudo systemctl start ntp
check_command "Starting NTP service"

echo "NTP update completed successfully."
