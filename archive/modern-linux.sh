#!/usr/bin/env bash

## install deb packages

mkdir /tmp/software
cd /tmp/software
curl -LO https://github.com/sharkdp/bat/releases/download/v0.18.1/bat_0.18.1_amd64.deb
curl -LO https://github.com/ClementTsang/bottom/releases/download/0.6.1/bottom_0.6.1_amd64.deb
curl -LO https://github.com/muesli/duf/releases/download/v0.6.2/duf_0.6.2_linux_amd64.deb
curl -LO https://github.com/sharkdp/fd/releases/download/v8.2.1/fd_8.2.1_amd64.deb
curl -LO https://github.com/sharkdp/hyperfine/releases/download/v1.11.0/hyperfine_1.11.0_amd64.deb
curl -LO https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb
curl -LO https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb
sudo apt install -y /tmp/software/*.deb
rm -rf /tmp/software/*.deb
