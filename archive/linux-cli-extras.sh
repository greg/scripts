#!/usr/bin/env bash
# set -ex

### tools

sudo apt install apt-utils autoconf automake ca-certificates bmon gzip httpie jq lnav lsof lynx make manpages ngrep nmap strace tree traceroute unrar unzip tcpdump whois zip

## tldr

sudo curl -o /usr/local/bin/tldr https://raw.githubusercontent.com/raylee/tldr/master/tldr
sudo chmod +x /usr/local/bin/tldr
