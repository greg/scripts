#!/usr/bin/env bash
# set -ex

### ubuntu/debian

sudo apt update && 
sudo apt install -y bash-completion curl git htop mc tmux tree vim

### 20.04

# sudo apt install bat fd-find fzf 

### rhel/centos (6+7)

# sudo yum install bash-completion curl git epel-release htop mc tmux vim 