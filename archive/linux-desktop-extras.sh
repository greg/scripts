#!/usr/bin/env bash
# set -ex

sudo apt update && 
sudo apt upgrade -y &&
sudo apt install -y apt-utils ffmpeg fonts-dejavu-core fonts-dejavu-extra fonts-open-sans fonts-roboto fonts-ubuntu fwupd fwupdate manpages redshift redshift-gtk rename sudo tmux tilix tree unrar unzip vim wget whois xclip zip
