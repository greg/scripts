#!/usr/bin/env bash

# This script sets the system timezone on debian-based systems.

# Exit immediately if a command exits with a non-zero status and handle errors gracefully
set -euo pipefail

# Ensure the script is being run with sudo or as root
if [[ "$(id -u)" -ne 0 ]]; then
    echo "Please execute this script with sudo or root privileges."
    exit 1
fi

# Check if the system is Debian-based
if ! grep -Ei 'debian|ubuntu|mint|kali|knoppix|mx|pureos|raspbian|siduction|sparkylinux|solydxk|steamos|devuan' /etc/os-release > /dev/null; then
    echo "This script is only compatible with Debian-based distributions."
    exit 1
fi

# Accept the TIMEZONE variable (if already set) or the command argument $1 as TIMEZONE (if provided).
# If neither is set, default to "America/Denver".
TIMEZONE="${1:-${TIMEZONE:-America/Denver}}"

# Constants
ZONEINFO_PATH="/usr/share/zoneinfo"
LOCALTIME_PATH="/etc/localtime"

# Set the DEBIAN_FRONTEND environment variable to suppress interactive prompts
export DEBIAN_FRONTEND=noninteractive

# Update the package index
echo "Updating package index..."
apt update

# Set the system timezone
echo "Setting system timezone to $TIMEZONE..."
ln -fs "$ZONEINFO_PATH/$TIMEZONE" "$LOCALTIME_PATH"

# Install and configure the tzdata package
echo "Installing tzdata package..."
apt-get install -y tzdata
dpkg-reconfigure --frontend noninteractive tzdata

echo "Script completed successfully!"
echo "System timezone set to $TIMEZONE."
