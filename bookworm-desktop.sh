#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# cli
sudo apt install -y apt-transport-https bash-completion bat busybox ca-certificates curl cpufrequtils duf exa fakeroot fd-find fzf git gnupg2 htop jq lsb-release mc ncdu openssl parallel pass vim ripgrep rsync software-properties-common tmux tree unzip wget


# # gui
# sudo apt install -y pavucontrol synaptic redshift

# # Install Brave browser
# sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
# echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
# sudo apt update
# sudo apt install -y brave-browser

# # Install Visual Studio Code
# wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
# sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
# sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
# rm -f packages.microsoft.gpg
# sudo apt update
# sudo apt install -y code

# # Install Docker
# sudo install -m 0755 -d /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# sudo chmod a+r /etc/apt/keyrings/docker.gpg
# echo \
#   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
# sudo apt update
# sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# # Install ASDF version manager
# git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.3
# echo '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc
# echo '. "$HOME/.asdf/completions/asdf.bash"' >> ~/.bashrc
# source ~/.bashrc

# # Install Node.js using ASDF
# asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git
# asdf install nodejs latest
# asdf global nodejs $(asdf latest nodejs)

# # Install Python using ASDF
# asdf plugin add python
# asdf install python latest
# asdf global python $(asdf latest python)

# # Install Rust
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# # Install Go
# asdf plugin add golang https://github.com/kennyp/asdf-golang.git
# asdf install golang latest
# asdf global golang $(asdf latest golang)

echo "Installation complete. Please restart your terminal for all changes to take effect."
