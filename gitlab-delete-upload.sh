#!/usr/bin/env bash
set -euo pipefail

# This function will be triggered if an ERR signal is caught. It prints a message about the error and the line number.
handle_error() {
    echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2
}

trap handle_error ERR

# Ensure `glab` is installed
if ! command -v glab &> /dev/null; then
    echo "Error: glab is not installed. Please install it first."
    exit 1
fi

# Ensure that the right number of arguments are passed
if [[ $# -ne 3 ]]; then
    echo "Usage: $0 <secret> <filename> <project_path>"
    exit 1
fi

secret="$1"
filename="$2"
project_path="$3"

# Use printf to safely generate the query string and avoid command injection
query=$(printf "mutation {uploadDelete(input: { secret: '%s', filename: '%s', projectPath: '%s'}) {upload {id size path} errors}}" "$secret" "$filename" "$project_path")

glab api -f query="$query"
