#!/usr/bin/env bash

# Exit on error, undefined variables, and pipe failures
set -euo pipefail

# Trap errors and print line number
trap 'echo "Error on line $LINENO" >&2' ERR

# Check if script is run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" >&2
    exit 1
fi

# Check if snapd is installed or running
if ! command -v snap >/dev/null 2>&1 && ! systemctl is-active --quiet snapd; then
    echo "Snapd is not running and snap packages are not installed. Nothing to do here."
    exit 0
fi

remove_snap_packages() {
    local packages
    if ! packages=$(snap list 2>/dev/null | awk 'NR>1 {print $1}'); then
        return 0
    fi
    
    if [[ -n "$packages" ]]; then
        echo "Removing snap packages..."
        echo "$packages" | while read -r package; do
            echo "Removing $package..."
            snap remove --purge "$package" || true
        done
    fi
}

remove_snapd() {
    echo "Stopping and disabling snapd services..."
    systemctl stop snapd.service snapd.socket snapd.seeded || true
    systemctl disable snapd.service snapd.socket snapd.seeded || true
    systemctl mask snapd.service || true
    
    echo "Removing snapd and cleaning up..."
    apt-get autoremove --purge snapd -y || true
    rm -rf /snap /var/snap /var/lib/snapd /var/cache/snapd /root/snap
}

prevent_snap_reinstall() {
    echo "Preventing snap from being reinstalled..."
    apt-mark hold snapd
    mkdir -p /etc/apt/preferences.d
    cat > /etc/apt/preferences.d/nosnap.pref << EOF
Package: snapd
Pin: release a=*
Pin-Priority: -10
EOF
}

echo "Starting Snap removal process..."

# Remove snap packages first
while snap list 2>/dev/null | grep -q '^[^N]'; do
    remove_snap_packages
    echo "Waiting for snap packages to be fully removed..."
    sleep 2
done

# Remove snapd and prevent reinstallation
remove_snapd
prevent_snap_reinstall

echo "Snap removal process completed successfully."
