#!/usr/bin/env bash

# This script automatically updates all git repositories present in the current directory.

set -uo pipefail

# Determine the base directory containing the repositories
if [[ -n "${BASE_DIR:-}" ]]; then
    : # BASE_DIR is already set, use it
elif [[ -n "${1:-}" ]]; then
    BASE_DIR="$1"
else
    BASE_DIR=$(pwd)
fi

# Update git repositories
update_git_repository() {
    local REPO_DIR="$1"
    
    echo "Updating $REPO_DIR..."

    # Change to the repository directory, pull updates, and handle potential errors
    if ! (cd "$REPO_DIR" && git pull); then
        echo "Failed to update $REPO_DIR. Continuing with the next repository."
        return
    fi

    echo "Successfully updated $REPO_DIR."
}

# Iterate over directories with a .git subdirectory and update them
find "$BASE_DIR" -type d -name ".git" -exec dirname {} \; | while read -r REPO_DIR; do
    update_git_repository "$REPO_DIR"
    echo
done
