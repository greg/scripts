#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Print commands and their arguments as they are executed
set -x

# Add Node.js plugin to ASDF
asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git

# Install latest version of Node.js
asdf install nodejs latest

echo "Node.js has been installed successfully using ASDF."

# Optionally, you can set the installed version as global
# Uncomment the following line if you want to set it as global
# asdf global nodejs $(asdf latest nodejs)

echo "You may want to set the installed version as global using:"
echo "asdf global nodejs \$(asdf latest nodejs)"

# Optionally, you can reshim Node.js to ensure the installed version is accessible
# Uncomment the following line if you want to reshim
# asdf reshim nodejs

echo "If you need to reshim Node.js, you can do so with:"
echo "asdf reshim nodejs"