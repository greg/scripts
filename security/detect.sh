#!/usr/bin/env bash
set -u
set -e
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
set -o E
set -o pipefail
IFS=$'\n\t'

find / -user git | egrep
crontab -u git -l
egrep /var/log/gitlab/nginx/gitlab_access.log
zgrep /var/log/gitlab/gitlab-workhorse/*s