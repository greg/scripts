#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status.
set -euo pipefail

trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

# Function to check if a command is available
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if necessary commands are available
for cmd in glab jq; do
  if ! command_exists "$cmd"; then
    printf "%s is not installed. Please install it first.\n" "$cmd"
    exit 1
  fi
done

# Function to prompt for PROJECT_ID if empty
get_project_id() {
  if [[ -z "${PROJECT_ID:-}" ]]; then
    printf "\033[1mPlease enter GitLab project ID to scan\033[0m\n"
    read -r -p "Project ID: " PROJECT_ID
  fi

  # Check if the variable PROJECT_ID is valid
  if [[ ! "$PROJECT_ID" =~ ^[1-9][0-9]{0,20}$ ]]; then
    printf "\033[1mERROR:\033[0m Project ID must be a number\n"
    exit 1
  fi
}

get_project_id

PROJECT_NAME=$(glab api "projects/$PROJECT_ID" | jq -r '.path')

project_ids=$(mktemp)
tags=$(mktemp)
images=$(mktemp)

echo "Enumerating images in $PROJECT_NAME"

glab api --paginate "projects/$PROJECT_ID/registry/repositories" | jq '.[].id' >> "$project_ids"

while read -r repository_id; do 
    glab api "projects/$PROJECT_ID/registry/repositories/$repository_id/tags" | jq -r '.[].name' >> "$tags" 
done < "project_ids.txt"

paste -d , "$project_ids" "$tags" >> "$images"

while IFS=',' read -r repository_id tag;
do
    glab api "projects/$PROJECT_ID/registry/repositories/$repository_id/tags/$tag" | tee -a "$PROJECT_NAME-registry.json"
done < "$images"
