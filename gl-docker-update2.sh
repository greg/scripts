#!/usr/bin/env bash

# This script stops the existing GitLab Docker container, pulls the latest image, 
# and then runs a new GitLab Docker container with the specified settings.

# Exit immediately if a command exits with a non-zero status and handle errors gracefully
set -euo pipefail

# Docker configuration parameters
CONTAINER_NAME="gitlab"
HOSTNAME="gitlab.example.com"
IMAGE="gitlab/gitlab-ee:latest"
SHM_SIZE="256m"

# Check if GITLAB_HOME is set or prompt user
if [[ -z "${GITLAB_HOME:-}" ]]; then
    echo "GITLAB_HOME not set. Would you like to use $DEFAULT_GITLAB_HOME? [Press Enter to confirm or type another path]"
    read -r input_path
    if [[ -z "$input_path" ]]; then
        GITLAB_HOME="$DEFAULT_GITLAB_HOME"
    else
        GITLAB_HOME="$input_path"
    fi
fi

# Stop and remove the existing GitLab Docker container
sudo docker stop "$CONTAINER_NAME" || echo "No running container named $CONTAINER_NAME to stop."
sudo docker rm "$CONTAINER_NAME" || echo "No container named $CONTAINER_NAME to remove."

# Pull the latest GitLab Docker image
sudo docker pull "$IMAGE"

# Run a new GitLab Docker container with the specified settings
sudo docker run --detach \
  --hostname "$HOSTNAME" \
  --publish 443:443 --publish 80:80 --publish 2222:22 \
  --name "$CONTAINER_NAME" \
  --restart always \
  --volume "$GITLAB_HOME/config:/etc/gitlab" \
  --volume "$GITLAB_HOME/logs:/var/log/gitlab" \
  --volume "$GITLAB_HOME/data:/var/opt/gitlab" \
  --shm-size "$SHM_SIZE" \
  "$IMAGE"
