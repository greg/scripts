#!/usr/bin/env bash

# This script automatically updates all git repositories present in the current directory
# and reports successful and failed operations.

set -euo pipefail

# Initialize arrays to store results
successful_updates=()
failed_updates=()

# Determine the base directory containing the repositories
if [[ -n "${BASE_DIR:-}" ]]; then
    : # BASE_DIR is already set, use it
elif [[ -n "${1:-}" ]]; then
    BASE_DIR="$1"
else
    BASE_DIR=$(pwd)
fi

# Update git repository and track the result
update_git_repository() {
    local repo_dir="$1"
    
    echo "Updating $repo_dir..."

    if (cd "$repo_dir" && git pull --ff-only); then
        echo "Successfully updated $repo_dir."
        successful_updates+=("$repo_dir")
    else
        echo "Failed to update $repo_dir. Continuing with the next repository."
        failed_updates+=("$repo_dir")
    fi
}

# Display the final report
display_report() {
    echo
    echo "=== Git Pull Report ==="
    echo "Successful updates (${#successful_updates[@]}):"
    for repo in "${successful_updates[@]}"; do
        echo " - $repo"
    done
    
    echo
    echo "Failed updates (${#failed_updates[@]}):"
    for repo in "${failed_updates[@]}"; do
        echo " - $repo"
    done
}

# Main execution
main() {
    local repo_dirs
    mapfile -t repo_dirs < <(find "$BASE_DIR" -type d -name ".git" -exec dirname {} \; | sort)

    for repo_dir in "${repo_dirs[@]}"; do
        update_git_repository "$repo_dir"
        echo
    done

    display_report
}

main

exit 0
