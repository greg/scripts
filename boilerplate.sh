#!/usr/bin/env bash

# Boilerplate for creating a simple bash script with some basic strictness

# Short form: set -u
set -o nounset

# Short form: set -e
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

# Function to check if a command is available
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# # Check if necessary commands are available
# for cmd in glab jq; do
#   if ! command_exists "$cmd"; then
#     printf "%s is not installed. Please install it first.\n" "$cmd"
#     exit 1
#   fi
# done
