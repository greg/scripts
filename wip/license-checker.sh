#!/usr/bin/env bash

TOKEN=
group_id=2504721
file_pattern='license|licenses'
license_pattern='gpl|mit'

search_group() {
  group_id=$1
  projects=`curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/groups/$group_id/projects?page=1" 2>/dev/null` #| jq -r '.[].path' | grep -i -E "$file_pattern"`
  project_ids=(`echo $projects | jq '.[].id'`)
  project_names=(`echo $projects | jq '.[].name' | sed 's/ /_/g'`)
  for i in ${!project_ids[@]}
  do
    project_id=${project_ids[i]}
    echo "searching project ${project_names[i]} ($project_id)"
    search_project $project_id
  done
}

search_project() {
  project_id=$1
  matching_files=`curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$project_id/repository/tree" 2>/dev/null | jq -r '.[].path' | grep -i -E "$file_pattern"`
  num=`echo -n $matching_files | wc -w`
  echo "searching project $project_id"
  echo "found $num files matching pattern '$file_pattern'"
  echo "-------"

  for i in `echo $matching_files`
  do
    content=`curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$project_id/repository/files/$i/raw?ref=master" 2> /dev/null`
    result=`echo $content | grep -i -E $license_pattern`
    echo "result of searching for '$license_pattern': in file $i"
    echo "$result"
    echo "-------"
  done
  echo
}

search_group $group_id
