#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

# Print commands and their arguments as they are executed
set -x

# Update package lists
sudo apt-get update

# Install dependencies
sudo apt-get install -y \
    autoconf \
    patch \
    build-essential \
    rustc \
    libssl-dev \
    libyaml-dev \
    libreadline6-dev \
    zlib1g-dev \
    libgmp-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm6 \
    libgdbm-dev \
    libdb-dev \
    uuid-dev

# Add Ruby plugin to ASDF
asdf plugin add ruby https://github.com/asdf-vm/asdf-ruby.git

# Install latest Ruby version
asdf install ruby latest

echo "Ruby dependencies installed, ASDF Ruby plugin added, and latest Ruby version installed successfully."

# Optionally, you can set the installed version as global
# Uncomment the following line if you want to set it as global
# asdf global ruby $(asdf latest ruby)

echo "You may want to set the installed Ruby version as global using:"
echo "asdf global ruby \$(asdf latest ruby)"

# Optionally, you can reshim Ruby to ensure the installed version is accessible
# Uncomment the following line if you want to reshim
# asdf reshim ruby

echo "If you need to reshim Ruby, you can do so with:"
echo "asdf reshim ruby"