#!/usr/bin/env bash

# Exit immediately if a command exits with a non-zero status
set -e
# Treat unset variables as an error and exit
set -u

# Ensure that the right number of arguments are passed
if [[ $# -ne 3 ]]; then
    echo "Usage: $0 <secret> <filename> <project_path>"
    exit 1
fi

secret="$1"
filename="$2"
project_path="$3"

# Use printf to safely generate the query string and avoid command injection
query=$(printf "mutation {uploadDelete(input: { secret: '%s', filename: '%s', projectPath: '%s'}) {upload {id size path} errors}}" "$secret" "$filename" "$project_path")

glab api -f query="$query"