#!/usr/bin/env bash

# This script stops the existing GitLab Docker container, pulls the latest image, 
# and then runs a new GitLab Docker container with the specified settings.

# Exit immediately if a command exits with a non-zero status and handle errors gracefully
set -euo pipefail

# Function to handle errors and provide clear feedback
handle_error() {
    echo "Error on line $LINENO. Exit code: $?" >&2
    exit 1
}

trap handle_error ERR

# Docker configuration parameters
CONTAINER_NAME="gitlab"
HOSTNAME="gitlab.example.com"
IMAGE="gitlab/gitlab-ee:latest"
SHM_SIZE="256m"

# Ensure the GITLAB_HOME environment variable is set
if [[ -z "${GITLAB_HOME:-}" ]]; then
    echo "Error: GITLAB_HOME environment variable is not set." >&2
    exit 1
fi

# Stop and remove the existing GitLab Docker container
sudo docker stop "$CONTAINER_NAME" || echo "No running container named $CONTAINER_NAME to stop."
sudo docker rm "$CONTAINER_NAME" || echo "No container named $CONTAINER_NAME to remove."

# Pull the latest GitLab Docker image
sudo docker pull "$IMAGE"

# Run a new GitLab Docker container with the specified settings
sudo docker run --detach \
  --hostname "$HOSTNAME" \
  --publish 443:443 --publish 80:80 --publish 2222:22 \
  --name "$CONTAINER_NAME" \
  --restart always \
  --volume "$GITLAB_HOME/config:/etc/gitlab" \
  --volume "$GITLAB_HOME/logs:/var/log/gitlab" \
  --volume "$GITLAB_HOME/data:/var/opt/gitlab" \
  --shm-size "$SHM_SIZE" \
  "$IMAGE"
