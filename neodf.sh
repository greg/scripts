#!/usr/bin/env bash

# neodf: A friendlier version of df.

set -euo pipefail

# Use mktemp to create a temporary file for the AWK script
AWK_SCRIPT=$(mktemp)

# Ensure the temporary AWK script is removed on exit
trap 'rm -f $AWK_SCRIPT' EXIT

# Define the AWK script as a here-document
cat << 'EOF' > "$AWK_SCRIPT"
function showunit(size) {
  mb = size / 1024
  prettymb = int(mb * 100) / 100
  gb = mb / 1024
  prettygb = int(gb * 100) / 100
  
  if (substr(size, 1, 1) !~ "[0-9]" || substr(size, 2, 1) !~ "[0-9]") {
    return size
  } else if (mb < 1) {
    return size "K"
  } else if (gb < 1) {
    return prettymb "M"
  } else {
    return prettygb "G"
  }
}

BEGIN {
  printf "%-37s %10s %7s %7s %8s %-s\n", "Filesystem", "Size", "Used", "Avail", "Capacity", "Mounted"
}

!/Filesystem/ {
  size = showunit($2)
  used = showunit($3)
  avail = showunit($4)
  printf "%-37s %10s %7s %7s %8s %-s\n", $1, size, used, avail, $5, $6
}
EOF

# Use df with the AWK script to produce the output
df -k | awk -f "$AWK_SCRIPT"
