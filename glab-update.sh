#!/bin/sh
# Default BINDIR=/usr/bin

set -euf

command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if necessary commands are available
for cmd in curl glab tar install; do
  if ! command_exists "$cmd"; then
    echo "$cmd is not installed. Please install it first."
    exit 1
  fi
done

if [ -n "${DEBUG-}" ]; then
    set -x
fi

: "${BINDIR:=/usr/bin}"

if [ $# -gt 0 ]; then
  BINDIR=$1
fi

# Function to check if we can install to the bindir
_can_install() {
  if [ ! -d "${BINDIR}" ]; then
    mkdir -p "${BINDIR}" 2> /dev/null
  fi
  [ -d "${BINDIR}" ] && [ -w "${BINDIR}" ]
}

# Validate if current user can install to bindir
if ! _can_install; then
  if [ "$(id -u)" != 0 ]; then
    echo "Directory ${BINDIR} is not writable. Try running this script with sudo."
    exit 1
  else
    echo "Can't install to ${BINDIR}. Please check the directory's permissions."
    exit 1
  fi
fi

machine=$(uname -m)
case ${machine} in
    aarch64)
        machine="arm64"
        ;;
esac

case $(uname -s) in
    Linux)
        os="Linux"
        ;;
    Darwin)
        os="macOS"
        ;;
    *)
        printf "OS not supported\n"
        exit 1
        ;;
esac

installed_version=$(glab --version | awk '{print $3}')
latest_version=$(curl -sL https://gitlab.com/api/v4/projects/34675721/releases/permalink/latest | grep --only-matching 'v[0-9\.]\+' | cut -c 2- | head -n 1)

printf "Fetching latest version\n"
# Compare the installed and the latest version
  if [ "$installed_version" == "$latest_version" ]
  then
      echo "You have the latest version of glab installed."
      exit
  else
      echo "Your glab version ($installed_version) is not up-to-date. The latest version is $latest_version."
      # Create a temporary directory using mktemp
      tempFolder=$(mktemp -d -t glab.XXXXXXXXXX)
      
      # Trap EXIT signal to ensure the temporary files are removed
      trap 'rm -rf "$tempFolder"' EXIT
      
      echo "Downloading glab_${latest_version}_${os}_${machine}.tar.gz"
      
      # Download and extract the tar.gz file
      if ! curl -sL "https://gitlab.com/api/v4/projects/34675721/releases/v${latest_version}/downloads/glab_${latest_version}_${os}_${machine}.tar.gz" | tar -C "${tempFolder}" -xzf -; then
        echo "Failed to download or extract glab_${latest_version}_${os}_${machine}.tar.gz"
        exit 1
      fi
      echo "Installing..."
      
      # Install the binary
      if ! install -m755 "${tempFolder}/bin/glab" "${BINDIR}/glab"; then
        echo "Failed to install glab to ${BINDIR}"
        exit 1
      fi

      echo "Successfully installed glab into ${BINDIR}/"
fi
