#!/usr/bin/env bash

# Enable error handling
# Enabling strict mode for better error handling
set -euo pipefail

# Uncomment the following line for debugging
# set -x

echo "This script will completely remove GitLab from your system."
read -p "Are you sure you want to continue? (y/n): " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Operation cancelled."
    exit 1
fi

echo "Stopping GitLab services..."
sudo gitlab-ctl stop
sudo gitlab-ctl cleanse
sudo systemctl stop gitlab-runsvdir
sudo systemctl disable gitlab-runsvdir

echo "Removing GitLab package..."
sudo apt purge gitlab-ee -y

echo "Removing GitLab directories and files..."
directories=(
    "/opt/gitlab/sv"
    "/opt/gitlab/init"
    "/opt/gitlab/etc"
    "/opt/gitlab/embedded/ssl/certs"
    "/opt/gitlab/embedded/service/gitlab-shell"
    "/opt/gitlab/embedded/service/gitlab-rails/public"
    "/opt/gitlab/embedded/service/gitlab-rails/config"
    "/opt/gitlab/embedded/service/gitlab-rails/changelogs/unreleased"
    "/opt/gitlab/embedded/service/gitlab-rails/app/workers"
    "/opt/gitlab/embedded/service/gitlab-rails/app/services/projects"
    "/opt/gitlab/embedded/cookbooks"
    "/opt/gitlab/embedded/bin"
)

for dir in "${directories[@]}"; do
    sudo rm -rf "$dir"
done

sudo rm -rf /etc/sysctl.d/*gitlab*
sudo rm -f /usr/lib/systemd/system/gitlab-runsvdir.service

echo "GitLab successfully removed."
echo "You'll need to run the following commands to install GitLab again:"
echo "sudo systemctl enable gitlab-runsvdir.service"
echo "sudo systemctl start gitlab-runsvdir.service"
